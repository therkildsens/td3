/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {
    int rep = 0;
    if (a>b){
        rep=a;
    }
    else{
        rep=b;
    }

    return rep;
    //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
}




/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */
int nbChiffresDuCarre(int nb){
    int rep = 0;
    nb = nb*nb;

    if (rep==0){
        return 1;
    }
    while (1<=nb){
        nb = nb/10;
        rep++;
    }
    return rep;
}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void repetCarac(int nb, char car){
    for (int i=0; i<=nb; i++){
        Ut.afficher(car);

    }
}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){
    for (int i=0;i==h;i++){
        repetCarac(i,c);
    }

}
void afficheNombresCroissants(int nb1, int nb2) {

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
    if(nb1<=nb2){
        for (int i=nb1; i<=nb2; i++)
            {
                Ut.afficher(i%10);
            }
    }

}
/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
    if(nb1<=nb2) {
        for (int i = nb2; i >= nb1; i--) {
            Ut.afficher(i % 10);
        }
    }
}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {


    for (int i=0;i<=h;i++) {
        repetCarac(h-i,' ');
        afficheNombresCroissants(i,i*2-1);
        //Ut.afficher(i);
        afficheNombresDecroissants(i,i*2-2);


        Ut.afficherSL("");
    }

}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
double racineParfaite(int c){
    double n = Math.pow(c, 0.5);

    if (c==n*n){
        return n;
    }
    return -1;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres(float n ){
    int add = 0;
    if (n==0){
        return 1;
    }

    while (1<=n){
        n = n/10;
        add++;
    }
    return add;
    //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 *
 * @param nb : double/int/float
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    if (racineParfaite(nb) != -1) {
        return true;
    }
    return false;

    //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 *
 * @param p : (int)
 * @param q : (int)
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){
    int diviseur_p = 0;
    int diviseur_q = 0;
    for (int i=1;i<p;i++) {
        if (p % i == 0) {
            diviseur_p += i;
        }
    }

    //Ut.afficher(p + " : " + diviseur_p);


    for (int i=1;i<q;i++) {
        if (q % i == 0) {
            diviseur_q += i;
        }
    }

    //Ut.afficher(q + " : " + diviseur_q);
    return (diviseur_q==p)&(diviseur_p==q);
    //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){
    for (int i = 1; i<=max;i++){
        for (int j=i+1;j<=max;j++){
            if (nombresAmicaux(i,j)){
                Ut.afficherSL(i + " : " + j);
            }
        }
    }
}

/**
 *
 * @param c1: int
 * @param c2: int
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){

    return Math.sqrt(c1*c1+c2*c2)%1==0;
    //throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}    //int c3 = ;

void trianglesRectanglesEntiers(int n){

    for (int i=0; i<=n; i++){
        for (int j=0; j<=i; j++){
            if (estTriangleRectangle(i, j)){
                Ut.afficherSL("triangle : c1="+i+" c2="+j+" c3="+(i*i+j*j));
            }
        }
    }

}

boolean estSyracusien(int n, int nbMaxOp){
    for (int i=0; i<=nbMaxOp; i++){
        if (n==1){
            return true;
        }
        else if (n%2==0){
            n = n/2;
        }
        else{
            n = 3*n + 1;
        }

    }
    return false;
}
void main (){
    //Ut.afficher(repetCarac(20,'*'));
    //pyramide(10);
    //Ut.afficher(nbChiffres(0));
    //afficheAmicaux(5000);
    //Ut.afficherSL(racineParfaite(4));
    //Ut.afficherSL(racineParfaite(2));
    //trianglesRectanglesEntiers(50);
   // Ut.afficherSL(estSyracusien(7,150));

}
